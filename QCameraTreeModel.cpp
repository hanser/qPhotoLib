#include "QCameraTreeModel.h"

#include <QCamera.h>

QCameraTreeModel::QCameraTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
{
}

QCameraTreeModel::~QCameraTreeModel()
{
    delete rootItem;
}

QModelIndex QCameraTreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    QCameraTreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<QCameraTreeItem*>(parent.internalPointer());

    QCameraTreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex QCameraTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    QCameraTreeItem *childItem = static_cast<QCameraTreeItem*>(index.internalPointer());
    QCameraTreeItem *parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int QCameraTreeModel::rowCount(const QModelIndex &parent) const
{
    QCameraTreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<QCameraTreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

int QCameraTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<QCameraTreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant QCameraTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    QCameraTreeItem *item = static_cast<QCameraTreeItem*>(index.internalPointer());

    return item->data(index.column());
}

Qt::ItemFlags QCameraTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant QCameraTreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

void QCameraTreeModel::setupModelData(QCamera *camera)
{
    this->camera = camera;
    CameraWidget * rootWidget = camera->getRootWidget();
    rootItem = new QCameraTreeItem(camera->getWidgetData(rootWidget));
    setupModelData(rootWidget, rootItem);
}

void QCameraTreeModel::setupModelData(CameraWidget * currentCameraWidget, QCameraTreeItem *current)
{
    int numberOfChildren = camera->getChildWidgetCount(currentCameraWidget);

    for (int childIndex=0; childIndex<numberOfChildren; childIndex++)
    {
        CameraWidget *child = camera->getChildWidget(currentCameraWidget, childIndex);

        int numberOfChildrenOfChild = camera->getChildWidgetCount(child);

        if(numberOfChildrenOfChild == 0)
        {
            current->appendChild(new QCameraTreeItem(camera->getLeafData(child), current));
        }
        else
        {
            current->appendChild(new QCameraTreeItem(camera->getWidgetData(child), current));
            //display_widgets (p, child, newprefix, dumpval);
            setupModelData(child, current->child(current->childCount()-1));
        }
    }
}

//################ QCameraTreeItem ######################

QCameraTreeItem::QCameraTreeItem(const QList<QVariant> &data, QCameraTreeItem *parent)
{
    m_parentItem = parent;
    m_itemData = data;
}

QCameraTreeItem::~QCameraTreeItem()
{
    qDeleteAll(m_childItems);
}

void QCameraTreeItem::appendChild(QCameraTreeItem *item)
{
    m_childItems.append(item);
}

QCameraTreeItem *QCameraTreeItem::child(int row)
{
    return m_childItems.value(row);
}

int QCameraTreeItem::childCount() const
{
    return m_childItems.count();
}

int QCameraTreeItem::row() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<QCameraTreeItem*>(this));

    return 0;
}

int QCameraTreeItem::columnCount() const
{
    return m_itemData.count();
}

QVariant QCameraTreeItem::data(int column) const
{
    return m_itemData.value(column);
}

QCameraTreeItem *QCameraTreeItem::parentItem()
{
    return m_parentItem;
}
