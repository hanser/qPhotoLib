#ifndef QCAMERATREEMODEL_H
#define QCAMERATREEMODEL_H

#include <QAbstractItemModel>
#include <QtCore>

#include <gphoto2/gphoto2-widget.h>

class QCamera;
class QCameraTreeItem;

class QCameraTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit QCameraTreeModel(QObject *parent = nullptr);
    ~QCameraTreeModel();

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QCameraTreeItem * getRootItem()
    {
        return rootItem;
    }
    void setupModelData(QCamera* camera);
private:
    void setupModelData(CameraWidget *currentCameraWidget, QCameraTreeItem *current);

    QCamera *camera;
    QCameraTreeItem *rootItem;
};

class QCameraTreeItem
{
public:
    explicit QCameraTreeItem(const QList<QVariant> &data, QCameraTreeItem *parentItem = 0);
    ~QCameraTreeItem();

    void appendChild(QCameraTreeItem *child);

    QCameraTreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    QCameraTreeItem *parentItem();

private:
    QList<QCameraTreeItem*> m_childItems;
    QList<QVariant> m_itemData;
    QCameraTreeItem *m_parentItem;
};

#endif // QCAMERATREEMODEL_H
