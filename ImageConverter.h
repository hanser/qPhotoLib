#ifndef IMAGECONVERTER_H
#define IMAGECONVERTER_H

#include <QImage>
#include <QPixmap>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

namespace ImageConverter
{
    inline QImage cvMatToQImage( const cv::Mat &inMat )
    {
        switch ( inMat.type() )
        {
            case CV_8UC4:
            {
                // 8-bit, 4 channel
                QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );
                return image;
            }
            case CV_8UC3:
            {
                // 8-bit, 3 channel
                QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );
                return image.rgbSwapped();
            }
            case CV_8UC1:
            {
                // 8-bit, 1 channel
                static QVector<QRgb>  sColorTable;

                // only create our color table once
                if ( sColorTable.isEmpty() )
                {
                    for ( int i = 0; i < 256; ++i )
                    {
                        sColorTable.push_back( qRgb( i, i, i ) );
                    }
                }

                QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );
                image.setColorTable( sColorTable );
                return image;
            }
            default:
                break;
        }
        return QImage();
    }

    inline QPixmap cvMatToQPixmap( const cv::Mat &inMat )
    {
        return QPixmap::fromImage( cvMatToQImage( inMat ) );
    }

    // If inImage exists for the lifetime of the resulting cv::Mat, pass false to inCloneImageData to share inImage's
    // data with the cv::Mat directly
    //    NOTE: Format_RGB888 is an exception since we need to use a local QImage and thus must clone the data regardless
    inline cv::Mat QImageToCvMat( const QImage &inImage, bool inCloneImageData = true )
    {
        switch ( inImage.format() )
        {
            case QImage::Format_RGB32:
            {
                // 8-bit, 4 channel
                cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

                return (inCloneImageData ? mat.clone() : mat);
            }
            case QImage::Format_RGB888:
            {
                // 8-bit, 3 channel
                QImage swapped = inImage.rgbSwapped();
                return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
            }
            case QImage::Format_Indexed8:
            {
                // 8-bit, 1 channel
                cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );
                return (inCloneImageData ? mat.clone() : mat);
            }
            default:
                break;
        }
        return cv::Mat();
    }

    // If inPixmap exists for the lifetime of the resulting cv::Mat, pass false to inCloneImageData to share inPixmap's data
    // with the cv::Mat directly
    //    NOTE: Format_RGB888 is an exception since we need to use a local QImage and thus must clone the data regardless
    inline cv::Mat QPixmapToCvMat( const QPixmap &inPixmap, bool inCloneImageData = true )
    {
        return QImageToCvMat( inPixmap.toImage(), inCloneImageData );
    }
}
#endif // IMAGECONVERTER_H
