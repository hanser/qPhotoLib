#ifndef IMAGETHREADOBJECT_H
#define IMAGETHREADOBJECT_H

#include <QObject>
#include <QImage>
#include <QMutex>
#include <QFileInfo>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class QCamera;
class QCameraTreeModel;

class QCameraThreadObject : public QObject
{
    Q_OBJECT
public:
    explicit QCameraThreadObject(bool dummy = false, QObject *parent = nullptr);
    ~QCameraThreadObject();

    QCameraTreeModel * getCameraTreeModel();
    void setRotation(int rotation = 0)
    {
        this->rotation = rotation;
    }
    bool incrementDummyPreviewNumber()
    {
        if(dummy)
        {
            QFileInfo check_file(QString("images/%1.jpg").arg(dummyPreviewNumber+1,5,10,QChar('0')));
            if (check_file.exists())
            {
                dummyPreviewNumber++;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
public slots:
    void slotTakePicture();
    void slotSetAperture(QString aperture);
    void slotSetShutterSpeed(QString shutterSpeed);
    void slotSetISO(QString iso);
    void slotSetFocusMode(QString focusMode);
    void slotExecuteFocus();
    void slotConnectCamera();
    void slotTakePreviewPicture();
    void slotLoadConfig();
    void slotExecutedFocus();
    void slotInitializePreview();
signals:
    void signalPictureTaken(QImage);
    void signalPreviewPictureTaken(QImage);
    void signalApertureValues(QStringList,int);
    void signalShutterSpeedValues(QStringList,int);
    void signalISOValues(QStringList,int);
    void signalFocusModeValues(QStringList,int);
    void signalSerialNumber(QString);
    void signalCameraOpened();
    void signalLoadedConfig();
    void signalExecutedFocus();
private:
    QImage* picture;
    QImage* previewPicture;
    QMutex *mutex;
    QCamera *camera;    
    int rotation;
    void recordImage(QImage &image);
    void recordPreviewImage(QImage &image);
    int canon_enable_capture(bool enable);
    bool dummy;
    int dummyPreviewNumber;
};

#endif // IMAGETHREADOBJECT_H
