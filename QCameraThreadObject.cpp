#include "QCameraThreadObject.h"

#include <QPainter>
#include <QtCore/qmath.h>
#include <QDir>
#include <QTime>
#include <QTimer>
#include <QDirIterator>
#include <qmath.h>

#include <QCamera.h>
#include <ImageConverter.h>

QCameraThreadObject::QCameraThreadObject(bool dummy, QObject *parent) :
    QObject(parent)
{
    mutex = new QMutex();
    // no rotation
    rotation = 0;
    dummyPreviewNumber = 0;

    this->dummy = dummy;

    if(!dummy)
    {
        camera = new QCamera();

        connect(camera,&QCamera::signalApertureValues,      this,&QCameraThreadObject::signalApertureValues);
        connect(camera,&QCamera::signalShutterSpeedValues,  this,&QCameraThreadObject::signalShutterSpeedValues);
        connect(camera,&QCamera::signalISOValues,           this,&QCameraThreadObject::signalISOValues);
        connect(camera,&QCamera::signalFocusModeValues,     this,&QCameraThreadObject::signalFocusModeValues);
        connect(camera,&QCamera::signalSerialNumber,        this,&QCameraThreadObject::signalSerialNumber);
    }
}

QCameraThreadObject::~QCameraThreadObject()
{
    mutex->lock();
    if(!dummy)
    {
        delete camera;
    }
    delete mutex;
}

QCameraTreeModel *QCameraThreadObject::getCameraTreeModel()
{
    return camera->getCameraTreeModel();
}

void QCameraThreadObject::slotConnectCamera()
{
    if(dummy || camera->open() == GP_OK)
    {
        emit signalCameraOpened();
    }
}

void QCameraThreadObject::slotTakePicture()
{
    if(mutex->tryLock(0))
    {
        QImage picture;
        recordImage(picture);

        if(rotation != 0 && rotation != 4)
        {
            QTransform transform;
            transform.rotate(rotation*90);
            picture = picture.transformed(transform);
        }

        emit signalPictureTaken(picture);

        mutex->unlock();
    }
}

void QCameraThreadObject::slotTakePreviewPicture()
{
    if(mutex->tryLock(0))
    {
        QImage previewPicture;
        if(dummy)
        {
            previewPicture.load(QString("images/%1").arg(dummyPreviewNumber,5,10,QChar('0')));
        }
        else
        {
            recordPreviewImage(previewPicture);
        }

        if(rotation != 0 && rotation != 4)
        {
            QTransform transform;
            transform.rotate(rotation*90);
            previewPicture = previewPicture.transformed(transform);
        }

        emit signalPreviewPictureTaken(previewPicture);
        mutex->unlock();
    }
}

void QCameraThreadObject::recordImage(QImage &image)
{
    QImage tempImage;
    if (camera->getInitialized() == GP_OK)
    {
        camera->recordImage(tempImage);
    }
    else
    {
        return;
    }

    image = tempImage;
}

void QCameraThreadObject::slotInitializePreview()
{
    camera->setConfigToggleValue("viewfinder", false);
}

void QCameraThreadObject::recordPreviewImage(QImage &image)
{
    QImage tempImage;
    if (camera->getInitialized() == GP_OK)
    {
        camera->recordPreviewImage(tempImage);
    }

    image = tempImage;
}

void QCameraThreadObject::slotSetAperture(QString aperture)
{
    camera->setConfigValueString("f-number", aperture.replace(QString("F "), "").toLocal8Bit());
}

void QCameraThreadObject::slotSetShutterSpeed(QString shutterSpeed)
{
    camera->setConfigValueString("shutterspeed", shutterSpeed.toLocal8Bit());
}

void QCameraThreadObject::slotSetISO(QString iso)
{
    camera->setConfigValueString("iso", iso.replace(QString("ISO "), "").toLocal8Bit());
}

void QCameraThreadObject::slotSetFocusMode(QString focusMode)
{
    camera->setConfigValueString("focusmode2", focusMode.toLocal8Bit());
}

void QCameraThreadObject::slotExecuteFocus()
{
    camera->setConfigToggleValue("autofocusdrive", -1);
    emit signalExecutedFocus();
    //QTimer::singleShot(3000, this, &CameraThreadObject::slotExecutedFocus);
}

void QCameraThreadObject::slotExecutedFocus()
{
    emit signalExecutedFocus();
}

void QCameraThreadObject::slotLoadConfig()
{
    camera->loadConfig();
    emit signalLoadedConfig();
}
