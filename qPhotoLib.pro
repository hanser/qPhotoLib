#-------------------------------------------------
#
# Project created by QtCreator 2016-09-27T20:59:16
#
#-------------------------------------------------

QT       -= core

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qPhotoLib
CONFIG(debug, debug|release) {
    TARGET = qPhotoLibd
}

TEMPLATE = lib

DEFINES += QPHOTOLIB_LIBRARY

SOURCES += QPhotoLib.cpp \
    QCameraThreadObject.cpp \
    QCamera.cpp \
    QCameraTreeModel.cpp

HEADERS += QPhotoLib.h\
    qphotolib_global.h \
    QCameraThreadObject.h \
    QCamera.h \
    QCameraTreeModel.h \
    ImageConverter.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

linux:!mac {
    # for debian
    INCLUDEPATH += /usr/include/opencv
    LIBS += -L/usr/lib/x86_64-linux-gnu/
    # for alpine
    INCLUDEPATH += /usr/local/include/opencv4
    LIBS += -L/usr/local/lib64/
    LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc
    LIBS += -lgphoto2
}
