#ifndef QCAMERA_H
#define QCAMERA_H

#include <QImage>

#include <gphoto2/gphoto2.h>
#include <gphoto2/gphoto2-camera.h>

class QCameraTreeModel;

class QCamera : public QObject
{
    Q_OBJECT
public:
    QCamera();
    ~QCamera();

    int getInitialized()
    {
        return initialized;
    }
    CameraWidget * getRootWidget()
    {
        return rootWidget;
    }
    QCameraTreeModel * getCameraTreeModel()
    {
        return cameraTreeModel;
    }

    int open();
    int recordImage(QImage &image);
    int recordPreviewImage(QImage &image);
    int setConfigValueString(const char *key, const char *val);
    int getConfigValueString(const char *key, char **str);
    int canon_enable_capture(bool enable);
    int _lookup_widget(const char *key, CameraWidget **child);
    int loadConfig();
    int saveConfig();
    QList<QVariant> getWidgetData(CameraWidget *current);
    int getChildWidgetCount(CameraWidget *current);
    CameraWidget* getChildWidget(CameraWidget *current, int childIndex);
    QList<QVariant> getLeafData(CameraWidget *current);
    int setConfigToggleValue(QString key, int enable);
signals:
    void signalApertureValues(QStringList,int);
    void signalShutterSpeedValues(QStringList,int);
    void signalISOValues(QStringList,int);
    void signalFocusModeValues(QStringList,int);
    void signalSerialNumber(QString);
private:
    int initialized;

    Camera	*camera;
    GPContext *cameraContext;

    CameraFile *file;

    CameraWidget *rootWidget;

    QCameraTreeModel *cameraTreeModel;
};

#endif // QCAMERA_H
