#include "QCamera.h"
#include <QTime>
#include <QComboBox>

#include <QCameraTreeModel.h>

QCamera::QCamera() :
    QObject()
{
    initialized = GP_ERROR;
    cameraTreeModel = nullptr;
    rootWidget = nullptr;

    cameraContext = gp_context_new();

    gp_camera_new(&camera);
    gp_file_new(&file);
}

QCamera::~QCamera()
{
    if(rootWidget != nullptr)
    {
        gp_widget_free (rootWidget);
    }

    gp_file_free(file);
    gp_camera_exit(camera, cameraContext);
    gp_camera_free(camera);
    if(cameraTreeModel != nullptr)
    {
        delete cameraTreeModel;
    }
}

int QCamera::open()
{
    if(initialized == GP_OK)
    {
        return GP_OK;
    }
#ifdef MAC
    system("killall PTPCamera");
#else
    system("killall gvfsd-gphoto2");
#endif
    initialized = gp_camera_init(camera, cameraContext);
    return initialized;
}

int QCamera::loadConfig()
{
    int returnValue = gp_camera_get_config (camera, &rootWidget, cameraContext);

    if(cameraTreeModel == nullptr)
    {
        cameraTreeModel = new QCameraTreeModel();
    }
    cameraTreeModel->setupModelData(this);

    return returnValue;
}

int QCamera::saveConfig()
{
    int returnValue = gp_camera_set_config (camera, rootWidget, cameraContext);
    return returnValue;
}

int QCamera::canon_enable_capture(bool enable)
{
    return setConfigToggleValue("capture", enable);
}

int QCamera::setConfigToggleValue(QString key, int enable)
{
    CameraWidget		*child = nullptr;
    CameraWidgetType	type;
    int			returnValue;

    returnValue = _lookup_widget (key.toLocal8Bit(), &child);
    if (returnValue < GP_OK)
    {
        return returnValue;
    }

    returnValue = gp_widget_get_type (child, &type);
    if (returnValue < GP_OK)
    {
        return returnValue;
    }

    switch (type)
    {
        case GP_WIDGET_TOGGLE:
            break;
        default:
            returnValue = GP_ERROR_BAD_PARAMETERS;
            return returnValue;
    }
    if(enable == -1)
    {
        returnValue = gp_widget_get_value (child, &enable);
        if (returnValue < GP_OK)
        {
            return returnValue;
        }
        enable++;
    }
    /* Now set the toggle to the wanted value */
    returnValue = gp_widget_set_value (child, &enable);
    if (returnValue < GP_OK)
    {
        return returnValue;
    }
    saveConfig();
    return GP_OK;
}

int QCamera::_lookup_widget(const char *key, CameraWidget **child)
{
    int returnValue;
    returnValue = gp_widget_get_child_by_name (rootWidget, key, child);
    if (returnValue < GP_OK)
        returnValue = gp_widget_get_child_by_label (rootWidget, key, child);
    return returnValue;
}

/* Gets a string configuration value.
 * This can be:
 *  - A Text widget
 *  - The current selection of a Radio Button choice
 *  - The current selection of a Menu choice
 *
 * Sample (for Canons eg):
 *   getConfigValueString (camera, "owner", &ownerstr, context);
 */
int QCamera::getConfigValueString (const char *key, char **str)
{
    CameraWidget		*child = nullptr;
    CameraWidgetType	type;
    int			returnValue;
    char			*val;

    returnValue = _lookup_widget (key, &child);
    if (returnValue < GP_OK)
    {
        qCritical() << "QC::getConfigValueString -- lookup widget failed:" << returnValue;
        return returnValue;
    }

    /* This type check is optional, if you know what type the label
     * has already. If you are not sure, better check. */
    returnValue = gp_widget_get_type (child, &type);
    if (returnValue < GP_OK)
    {
        qCritical() << "QC::getConfigValueString -- widget get type failed:" << returnValue;
        return returnValue;
    }
    switch (type) {
        case GP_WIDGET_MENU:
        case GP_WIDGET_RADIO:
        case GP_WIDGET_TEXT:
        break;
    default:
        qCritical() << "QC::getConfigValueString -- widget has bad type" << type;
        returnValue = GP_ERROR_BAD_PARAMETERS;
        return returnValue;
    }

    /* This is the actual query call. Note that we just
     * a pointer reference to the string, not a copy... */
    returnValue = gp_widget_get_value (child, &val);
    if (returnValue < GP_OK)
    {
        qCritical() << "QC::getConfigValueString -- could not query widget value:" << returnValue;
        return returnValue;
    }
    /* Create a new copy for our caller. */
    *str = strdup (val);
    return returnValue;
}


/* Sets a string configuration value.
 * This can set for:
 *  - A Text widget
 *  - The current selection of a Radio Button choice
 *  - The current selection of a Menu choice
 *
 * Sample (for Canons eg):
 *   setConfigValueString (camera, "owner", &ownerstr, context);
 */
int QCamera::setConfigValueString (const char *key, const char *val)
{
    CameraWidget *child = nullptr;
    int returnValue;
    returnValue = _lookup_widget (key, &child);
    if (returnValue < GP_OK)
    {
        qCritical() << "QC::setConfigValueString -- lookup widget failed:" << returnValue;
        return returnValue;
    }

    /* This type check is optional, if you know what type the label
     * has already. If you are not sure, better check. */
    CameraWidgetType type;
    returnValue = gp_widget_get_type (child, &type);
    if (returnValue < GP_OK)
    {
        qCritical() << "QC::setConfigValueString -- widget get type failed:" << returnValue;
        return returnValue;
    }
    switch (type)
    {
        case GP_WIDGET_MENU:
        case GP_WIDGET_RADIO:
        case GP_WIDGET_TEXT:
            /* This is the actual set call. Note that we keep
             * ownership of the string and have to free it if necessary. */
            returnValue = gp_widget_set_value (child, val);
            if (returnValue < GP_OK)
            {
                qCritical() << "QC::setConfigValueString -- could not set widget value:" << returnValue;
                return returnValue;
            }
            break;
        case GP_WIDGET_TOGGLE:
        {
            int ival;
            sscanf(val,"%d",&ival);
            returnValue = gp_widget_set_value (child, &ival);
            if (returnValue < GP_OK)
            {
                qCritical() << "QC::setConfigValueString -- could not set widget value:" << returnValue;
                return returnValue;
            }
            break;
        }
        default:
            qCritical() << "QC::setConfigValueString -- widget has bad type" << type;
            returnValue = GP_ERROR_BAD_PARAMETERS;
            return returnValue;
    }

    saveConfig();
    return returnValue;
}

int QCamera::recordImage(QImage &image)
{
    char *data;
    unsigned long size;
    CameraFilePath camera_file_path;
    int returnValue;
    Q_UNUSED(returnValue);

    // NOP: This gets overridden in the library to /capt0000.jpg
    strcpy(camera_file_path.folder, "/");
    strcpy(camera_file_path.name, "0000capt.jpg");

    //returnValue = canon_enable_capture (false);

    returnValue = gp_camera_capture(camera, GP_CAPTURE_IMAGE, &camera_file_path, cameraContext);

    returnValue = gp_camera_file_get(camera, camera_file_path.folder, camera_file_path.name, GP_FILE_TYPE_NORMAL, file, cameraContext);

    returnValue = gp_file_get_data_and_size (file, (const char**)&data, &size);

    returnValue = gp_camera_file_delete(camera, camera_file_path.folder, camera_file_path.name, cameraContext);

//    std::vector<char> datastream(data, data + size);
//    cv::Mat img = cv::imdecode(cv::Mat(datastream),CV_LOAD_IMAGE_COLOR);

    image = QImage::fromData(reinterpret_cast<const unsigned char*>(data), size);

    return GP_OK;
}

int QCamera::recordPreviewImage(QImage &image)
{
    char *data;
    unsigned long size;
    int returnValue;
    Q_UNUSED(returnValue);

    //returnValue = canon_enable_capture (false);

    returnValue = gp_camera_capture_preview(camera, file, cameraContext);

    //CameraFileType *type;
    //returnValue = gp_file_get_type (file, type);

    gp_file_get_data_and_size (file, (const char**)&data, &size);

    image = QImage::fromData(reinterpret_cast<const unsigned char*>(data), size);

    return GP_OK;
}

QList<QVariant> QCamera::getWidgetData(CameraWidget *current)
{
    QList<QVariant> currentData;
    const char *label, *name;
    CameraWidgetType	type;
    gp_widget_get_label (current, &label);
    gp_widget_get_name (current, &name);
    gp_widget_get_type (current, &type);
    currentData << label << name << type;
    return currentData;
}

QList<QVariant> QCamera::getLeafData(CameraWidget *current)
{
    QList<QVariant> currentData;
    const char *name;
    CameraWidgetType type;
    gp_widget_get_type (current, &type);
    gp_widget_get_name (current, &name);
    switch (type)
    {
        case GP_WIDGET_TEXT:
        {   /* char *		*/
            char *txt;
            gp_widget_get_value (current, &txt);
            if(QString(name).compare("serialnumber") == 0)
            {
                emit signalSerialNumber(QString(txt));
            }
            currentData << name << txt;
            break;
        }
        case GP_WIDGET_RANGE:
        {	/* float		*/
            float t,b,s;
            gp_widget_get_range (current, &b, &t, &s);
            currentData << name << QString("%1:%2:%3").arg(b,s,t);
            break;
        }
        case GP_WIDGET_TOGGLE:
        {	/* int		*/
            int	t;
            gp_widget_get_value (current, &t);
            currentData << name << t;
            break;
        }
        case GP_WIDGET_DATE:
        {	/* int
            int	t;
            time_t	xtime;
            struct tm *xtm;
            char	timebuf[200];

            ret = gp_widget_get_value (widget, &t);
            if (ret != GP_OK) {
                gp_context_error (p->context, _("Failed to retrieve values of date/time widget %s."), name);
                break;
            }
            xtime = t;
            xtm = localtime (&xtime);
            ret = my_strftime (timebuf, sizeof(timebuf), "%c", xtm);
            printf ("Type: DATE\n");
            printf ("Current: %d\n", t);
            printf ("Printable: %s\n", timebuf);
            printf ("Help: %s\n", _("Use 'now' as the current time when setting.\n"));
            */
            currentData << "DATE" << "TODO";
            break;
        }
        case GP_WIDGET_MENU:
        case GP_WIDGET_RADIO: { /* char *		*/
            char *currentValue;

            gp_widget_get_value (current, &currentValue);

            QStringList optionList;
            QString options;
            int cnt, i, currentIndex = -1;
            cnt = gp_widget_count_choices (current);
            for ( i=0; i<cnt; i++) {
                const char *choice;
                gp_widget_get_choice (current, i, &choice);

                if(QString(name).compare("f-number") == 0)
                {
                    optionList.append(QString("F %1").arg(QString().fromLatin1(choice).replace(QString("f/"),"")));
                }
                else if(QString(name).compare("shutterspeed") == 0)
                {
                    optionList.append(QString("%1").arg(choice));
                }
                else if(QString(name).compare("iso") == 0)
                {
                    optionList.append(QString("ISO %1").arg(choice));
                }
                else
                {
                    optionList.append(choice);
                }
                if(QString::compare(choice, currentValue) == 0)
                {
                    currentIndex = i;
                }
                if(i == 0)
                {
                    options += QString("%1").arg(choice);
                }
                else
                {
                    options += QString(";%1").arg(choice);
                }
            }
            if(QString(name).compare("f-number") == 0)
            {
                emit signalApertureValues(optionList,currentIndex);
            }
            else if(QString(name).compare("shutterspeed") == 0)
            {
                emit signalShutterSpeedValues(optionList,currentIndex);
            }
            else if(QString(name).compare("iso") == 0)
            {
                emit signalISOValues(optionList,currentIndex);
            }
            else if(QString(name).compare("focusmode2") == 0)
            {
                emit signalFocusModeValues(optionList,currentIndex);
            }

            currentData << name << currentValue << options;
            break;
        }

        /* ignore: */
        case GP_WIDGET_WINDOW:
        case GP_WIDGET_SECTION:
        case GP_WIDGET_BUTTON:
            break;
    }
    return currentData;
}

int QCamera::getChildWidgetCount(CameraWidget *current)
{
    return gp_widget_count_children(current);
}

CameraWidget * QCamera::getChildWidget(CameraWidget *current, int childIndex)
{
    CameraWidget *child;
    gp_widget_get_child (current, childIndex, &child);
    return child;
}

