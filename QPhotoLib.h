#ifndef QPHOTOLIB_H
#define QPHOTOLIB_H

#include "qphotolib_global.h"

#include <QThread>
#include <QStringList>
#include <QTimer>
#include <QImage>

class QCameraThreadObject;
class QCameraTreeModel;

class QPHOTOLIBSHARED_EXPORT QPhotoLib : public QObject
{
    Q_OBJECT
public:
    QPhotoLib(bool dummy = false);
    ~QPhotoLib();

    void startPreview(int delayms = 50);
    void stopPreview();
    void takePicture();

    QCameraTreeModel * getCameraTreeModel();

    void setRotation(int rotation = 0);
    bool incrementDummyPreviewNumber();

    void setAperture(QString value);
    void setShutterSpeed(QString value);
    void setISO(QString value);
    void setFocusMode(QString value);
    void executeFocus();

signals:
    void pictureTaken(QImage);
    void previewPictureTaken(QImage);
    void startingPreview(int);
    void takingPicture();

    void cameraConnected();
    void cameraDisconnected();

    void apertureValues(QStringList,int);
    void shutterSpeedValues(QStringList,int);
    void iSOValues(QStringList,int);
    void focusModeValues(QStringList,int);
    void serialNumber(QString);

    void signalSetAperture(const QString &);
    void signalSetShutterSpeed(QString);
    void signalSetISO(QString);
    void signalSetFocusMode(QString);
    void signalExecuteFocus();
    void signalLoadConfig();
    void signalExecutedFocus();
    void signalInitializePreview();
private slots:
    void slotCameraOpened();
private:
    QThread *imageThread;
    QCameraThreadObject *cameraThreadObject;
    QTimer cameraReconnectionTimer;
    QTimer previewTimer;
};

Q_DECLARE_METATYPE(QImage);

#endif // QPHOTOLIB_H
