#include "QPhotoLib.h"

#include <QMetaType>
#include <QCameraThreadObject.h>

QPhotoLib::QPhotoLib(bool dummy)
{
    imageThread = nullptr;
    cameraThreadObject = nullptr;

    qRegisterMetaType<QImage>();

    imageThread = new QThread();
    imageThread->start();
    connect(imageThread, &QThread::finished, imageThread, &QThread::deleteLater);

    cameraThreadObject = new QCameraThreadObject(dummy);
    cameraThreadObject->moveToThread(imageThread);

    connect(&cameraReconnectionTimer, &QTimer::timeout, cameraThreadObject, &QCameraThreadObject::slotConnectCamera,      Qt::QueuedConnection);
    connect(&previewTimer,            &QTimer::timeout, cameraThreadObject, &QCameraThreadObject::slotTakePreviewPicture, Qt::QueuedConnection);

    cameraReconnectionTimer.start(1000);

    if(!dummy)
    {
        connect(this,&QPhotoLib::takingPicture,          cameraThreadObject,&QCameraThreadObject::slotTakePicture,Qt::QueuedConnection);
        connect(this,&QPhotoLib::signalSetAperture,      cameraThreadObject,&QCameraThreadObject::slotSetAperture,Qt::QueuedConnection);
        connect(this,&QPhotoLib::signalSetShutterSpeed,  cameraThreadObject,&QCameraThreadObject::slotSetShutterSpeed,Qt::QueuedConnection);
        connect(this,&QPhotoLib::signalSetISO,           cameraThreadObject,&QCameraThreadObject::slotSetISO,Qt::QueuedConnection);
        connect(this,&QPhotoLib::signalSetFocusMode,     cameraThreadObject,&QCameraThreadObject::slotSetFocusMode,Qt::QueuedConnection);
        connect(this,&QPhotoLib::signalExecuteFocus,     cameraThreadObject,&QCameraThreadObject::slotExecuteFocus,Qt::QueuedConnection);
        connect(this,&QPhotoLib::signalLoadConfig,       cameraThreadObject,&QCameraThreadObject::slotLoadConfig,Qt::QueuedConnection);
        connect(this,&QPhotoLib::signalInitializePreview,cameraThreadObject,&QCameraThreadObject::slotInitializePreview,Qt::QueuedConnection);

        connect(cameraThreadObject,&QCameraThreadObject::signalPictureTaken,       this,&QPhotoLib::pictureTaken,Qt::QueuedConnection);

        connect(cameraThreadObject,&QCameraThreadObject::signalApertureValues,     this,&QPhotoLib::apertureValues,Qt::QueuedConnection);
        connect(cameraThreadObject,&QCameraThreadObject::signalShutterSpeedValues, this,&QPhotoLib::shutterSpeedValues,Qt::QueuedConnection);
        connect(cameraThreadObject,&QCameraThreadObject::signalISOValues,          this,&QPhotoLib::iSOValues,Qt::QueuedConnection);
        connect(cameraThreadObject,&QCameraThreadObject::signalFocusModeValues,    this,&QPhotoLib::focusModeValues,Qt::QueuedConnection);
        connect(cameraThreadObject,&QCameraThreadObject::signalSerialNumber,       this,&QPhotoLib::serialNumber,Qt::QueuedConnection);
        connect(cameraThreadObject,&QCameraThreadObject::signalExecutedFocus,      this,&QPhotoLib::signalExecutedFocus,Qt::QueuedConnection);
    }
    connect(cameraThreadObject, &QCameraThreadObject::signalPreviewPictureTaken, this, &QPhotoLib::previewPictureTaken, Qt::QueuedConnection);
    connect(cameraThreadObject, &QCameraThreadObject::signalCameraOpened,        this, &QPhotoLib::slotCameraOpened,    Qt::QueuedConnection);
    connect(cameraThreadObject, &QCameraThreadObject::signalLoadedConfig,        this, &QPhotoLib::cameraConnected,     Qt::QueuedConnection);
}

QPhotoLib::~QPhotoLib()
{
    if(imageThread != nullptr)
    {
        imageThread->quit();
    }

    if(cameraThreadObject != nullptr)
    {
        delete cameraThreadObject;
        cameraThreadObject = nullptr;
    }
}

void QPhotoLib::startPreview(int delayms)
{
    emit signalInitializePreview();
    previewTimer.start(delayms);
}

void QPhotoLib::stopPreview()
{
    previewTimer.stop();
}

void QPhotoLib::takePicture()
{
    emit takingPicture();
}

void QPhotoLib::setAperture(QString value)
{
    emit signalSetAperture(value);
}
void QPhotoLib::setShutterSpeed(QString value)
{
    emit signalSetShutterSpeed(value);
}
void QPhotoLib::setISO(QString value)
{
    emit signalSetISO(value);
}
void QPhotoLib::setFocusMode(QString value)
{
    emit signalSetFocusMode(value);
}
void QPhotoLib::executeFocus()
{
    emit signalInitializePreview();
    previewTimer.stop();
    emit signalExecuteFocus();
}

QCameraTreeModel *QPhotoLib::getCameraTreeModel()
{
    return cameraThreadObject->getCameraTreeModel();
}

void QPhotoLib::setRotation(int rotation)
{
    cameraThreadObject->setRotation(rotation);
}

bool QPhotoLib::incrementDummyPreviewNumber()
{
    return cameraThreadObject->incrementDummyPreviewNumber();
}

void QPhotoLib::slotCameraOpened()
{
    cameraReconnectionTimer.stop();
    emit signalLoadConfig();
}
